<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>

  
  <header class="site-header">
    <div class="container">
    <a href="<?php echo site_url() ?>" class="logo"><p class="school-logo-text float-left">Organomistrzostwo<br><span style="font-size:14px; font-weight:200;">Piotr Dykas</span></p></a>
      <i class="site-header__menu-trigger fa fa-bars" aria-hidden="true"></i>
      <div class="site-header__menu group">
        <nav class="nav main-navigation">
          <ul>
            <?php
              wp_nav_menu(array(
                'theme_location' => 'headerMenuLocation'
              )); 
            ?>
          </ul>
        </nav>
      </div>
    </div>
  </header>


 