<?php
  $main_img = get_field('main_img');
  $main_title = get_field('main_title');
?>

<section class="page-banner" style="background-image: url(<?= $main_img ?>);" id="home" >
    <div class="page-banner__overlay">
        <div id="intro" class="">
            <div class="container">
                <h4 class="page-banner--description" data-aos="zoom-in" data-aos-duration="2000">
                    <?= $main_title ?>
                </h4>
            </div>
            </div>
        </div>
    </div>
</section>