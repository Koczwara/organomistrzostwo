<?php
  $about_us_point_one = get_field('about_us_point_one');
  $about_us_point_two = get_field('about_us_point_two');
  $about_us_point_three = get_field('about_us_point_three');
  $about_us_button = get_field('about_us_button');
?>

<div class="container about-us-main">
    <div class="main-about-us">
            <div class="col-4 main-about-us--point" data-aos="zoom-in" data-aos-duration="2000">
                <img class="main-about-us--point--img" src="<?php echo get_theme_file_uri('/images/check.png')?>" alt="check" loading="lazy">
                <p>
                    <?= $about_us_point_one ?>
                </p>
            </div>
            <div class="col-4 main-about-us--point" data-aos="zoom-in" data-aos-duration="2000">
                <img class="main-about-us--point--img" src="<?php echo get_theme_file_uri('/images/check.png')?>" alt="check" loading="lazy">
                <p>
                    <?= $about_us_point_two ?>
                </p>
            </div>
            <div class="col-4 main-about-us--point" data-aos="zoom-in" data-aos-duration="2000">
                <img class="main-about-us--point--img" src="<?php echo get_theme_file_uri('/images/check.png')?>" alt="check" loading="lazy">
                <p>
                    <?= $about_us_point_three ?>
                </p>
            </div>
    </div>
    <div class="button-container">
    <a href="<?php echo site_url('/o-nas')?>">
        <button class="main-about-us--button">
            <?= $about_us_button ?>
        </button>
    </a>
    </div>
</div>

