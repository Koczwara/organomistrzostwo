<?php
  $about_us_title = get_field('about_us_title');
  $about_us_description = get_field('about_us_description');
  $about_us_main_img = get_field('about_us_main_img');
  $about_us_img_one = get_field('about_us_img_one');
  $about_us_img_two = get_field('about_us_img_two');
?>

<div class="banner">
    <div class="banner__bg-image" style="background-image: url(<?= $about_us_main_img ?>);"></div>
    <div class="banner__content container container--narrow">
    <h1 class="banner__title">
        <?= $about_us_title ?>
    </h1>
    </div>  
</div>
<div class="about-us container">
    <div class="row">
        <div class="col">
            <p class="about-us--description" data-aos="zoom-in" data-aos-duration="2000">
                <?= $about_us_description ?>
            </p>
        </div>
    </div>
    <div class="row about-us--dyplomy">
        <div class="col-md-6">
            <img class="about-us--img" src="<?= $about_us_img_one ?>">
        </div>
        <div class="col-md-6">
            <img class="about-us--img" src="<?= $about_us_img_two ?>">
        </div>
    </div>
</div>