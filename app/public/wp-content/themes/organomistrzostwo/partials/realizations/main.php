<?php
  $realizations_title = get_field('realizations_title');
  $main_img = get_field('main_img');
  $img_one= get_field('img_one');
  $img_two= get_field('img_two');
  $img_three= get_field('img_three');
  $img_four= get_field('img_four');
  $img_five= get_field('img_five');
  $img_six= get_field('img_six');
  $img_seven= get_field('img_seven');
  $img_eight= get_field('img_eight');
  $img_nine= get_field('img_nine');
?>

<div class="banner">
    <div class="banner__bg-image" style="background-image: url(<?= $main_img ?>);"></div>
    <div class="banner__content container container--narrow">
        <h1 class="banner__title">
            <?= $realizations_title ?>
        </h1>
    </div>  
</div>
<div class="container container--narrow page-section">  
    <section id="gallery">
        <div class="container">
            <div id="image-gallery" class="image-gallery" data-aos="zoom-in" data-aos-duration="2000">
            <?php if (!empty($img_one)) { ?>
                <div class="image">
                <div class="img-wrapper">
                <a href="<?= $img_one ?>">
                    <img src="<?= $img_one ?>" class="img-responsive" loading="lazy">
                </a>
                    <div class="img-overlay">
                    </div>
                </div>
                </div>
            <?php } ?>
            <?php if (!empty($img_two)) { ?>
                <div class="image">
                <div class="img-wrapper">
                    <a href="<?= $img_two ?>">
                        <img src="<?= $img_two ?>" class="img-responsive" loading="lazy">
                    </a>
                    <div class="img-overlay">
                    </div>
                </div>
                </div>
            <?php } ?>
            <?php if (!empty($img_three)) { ?>
                <div class="image">
                <div class="img-wrapper">
                    <a href="<?= $img_three ?>">
                        <img src="<?= $img_three?>" class="img-responsive" loading="lazy">
                    </a>
                    <div class="img-overlay">
                    </div>
                </div>
                </div>
            <?php } ?>
            <?php if (!empty($img_four)) { ?>
                <div class="image">
                <div class="img-wrapper">
                    <a href="<?= $img_four ?>">
                        <img src="<?= $img_four ?>" class="img-responsive" loading="lazy">
                    </a>
                    <div class="img-overlay">
                    </div>
                </div>
                </div>
            <?php } ?>
            <?php if (!empty($img_five)) { ?>
                <div class="image">
                <div class="img-wrapper">
                    <a href="<?= $img_five ?>">
                        <img src="<?= $img_five ?>" class="img-responsive" loading="lazy">
                    </a>
                    <div class="img-overlay">
                    </div>
                </div>
                </div>
            <?php } ?>
            <?php if (!empty($img_six)) { ?>
                <div class="image">
                <div class="img-wrapper">
                    <a href="<?= $img_six ?>">
                        <img src="<?= $img_six ?>" class="img-responsive" loading="lazy">
                    </a>
                    <div class="img-overlay">
                    </div>
                </div>
                </div>
            <?php } ?>
            <?php if (!empty($img_seven)) { ?>
                <div class="image">
                <div class="img-wrapper">
                    <a href="<?= $img_seven ?>">
                        <img src="<?= $img_seven ?>" class="img-responsive" loading="lazy">
                    </a>
                    <div class="img-overlay">
                    </div>
                </div>
                </div>
            <?php } ?>
            <?php if (!empty($img_eight)) { ?>
                <div class="image">
                <div class="img-wrapper">
                    <a href="<?= $img_eight ?>">
                        <img src="<?= $img_eight ?>" class="img-responsive" loading="lazy">
                    </a>
                    <div class="img-overlay">
                    </div>
                </div>
                </div>
            <?php } ?>
            <?php if (!empty($img_nine)) { ?>
                <div class="image">
                <div class="img-wrapper">
                    <a href="<?= $img_nine ?>">
                        <img src="<?= $img_nine ?>" class="img-responsive" loading="lazy">
                    </a>
                    <div class="img-overlay">
                    </div>
                </div>
                </div>
            <?php } ?>
            </div>
        </div>
    </section>           
</div>

        
