<?php
  $offer_title = get_field('offer_title');
  $offer_point_one = get_field('offer_point_one');
  $offer_point_two = get_field('offer_point_two');
  $offer_point_three = get_field('offer_point_three');
  $offer_point_four = get_field('offer_point_four');
  $offer_point_five = get_field('offer_point_five');
  $offer_point_six = get_field('offer_point_six');
  $offer_point_seven = get_field('offer_point_seven');
  $offer_point_eight = get_field('offer_point_eight');
?>

<div class="slider-container">
   <h1 class="offer-title">
      <?= $offer_title ?>
   </h1>
   <div class="glide hero-slider container" id="glide1">
         <div class="glide__track" data-glide-el="track">
            <ul class="glide__slides">
            <?php if (!empty($offer_point_one)) { ?>
               <li class="hero-slider__slide">
                  <div class="hero-slider__box hero-slider__box--small">
                     <p class="hero-slider__description">
                        <?= $offer_point_one ?>
                     </p>
                  </div>
               </li>
            <?php } ?>
            <?php if (!empty($offer_point_two)) { ?>
               <li class="hero-slider__slide">
                     <div class="hero-slider__box hero-slider__box--big">
                        <p class="hero-slider__description">
                           <?= $offer_point_two ?>
                        </p>
                     </div>
               </li>
            <?php } ?>
            <?php if (!empty($offer_point_three)) { ?>
               <li class="hero-slider__slide">
                  <div class="hero-slider__box hero-slider__box--small">
                     <p class="hero-slider__description">
                        <?= $offer_point_three ?>
                     </p>
                     </div>
                  </li>
            <?php } ?>
            <?php if (!empty($offer_point_four)) { ?>
               <li class="hero-slider__slide">
                  <div class="hero-slider__box hero-slider__box--big">
                     <p class="hero-slider__description">
                        <?= $offer_point_four ?>
                     </p>
                  </div>
               </li>
            <?php } ?>
            <?php if (!empty($offer_point_five)) { ?>
               <li class="hero-slider__slide">
                     <div class="hero-slider__box hero-slider__box--small">
                        <p class="hero-slider__description">
                           <?= $offer_point_five ?>
                        </p>
                     </div>
               </li>
            <?php } ?>
            <?php if (!empty($offer_point_six)) { ?>
               <li class="hero-slider__slide">
                  <div class="hero-slider__box hero-slider__box--big">
                     <p class="hero-slider__description">
                        <?= $offer_point_six ?>
                     </p>
                  </div>
               </li>
            <?php } ?>
            <?php if (!empty($offer_point_seven)) { ?>
               <li class="hero-slider__slide">
                  <div class="hero-slider__box hero-slider__box--small">
                     <p class="hero-slider__description">
                        <?= $offer_point_seven ?>
                     </p>
                  </div>
               </li>
            <?php } ?>
            <?php if (!empty($offer_point_eight)) { ?>
               <li class="hero-slider__slide">
                     <div class="hero-slider__box hero-slider__box--big">
                        <p class="hero-slider__description">
                           <?= $offer_point_eight ?>
                        </p>
                     </div>
               </li>
            <?php } ?>
         </ul>
      </div> 
   </div>
  </div>