<?php
  $privacy_policy = get_field('privacy_policy');
  $privacy_policy_link = get_field('privacy_policy_link');
  $regulations = get_field('regulations');
  $regulations_link = get_field('regulations_link');
  $mail_adress = get_field('mail_adress');
  $phone_number = get_field('phone_number');
  $facebook_link = get_field('facebook_link');
?>


<footer class="site-footer" id="footer">
   <div class="site-footer__inner container">
        <div class="group">
         <div class="site-footer__col-one">        
         <h1 class="school-logo-text--footer">
         <a href="<?php echo site_url() ?>" class="logo"><p class="school-logo-text float-left">Organomistrzostwo<br><span style="font-size:14px; font-weight:200;">Piotr Dykas</span></p></a>
          </h1>
        </div>

        <div class="site-footer__col-two">
            <h6 class="site-footer--title">Linki</h6>
              <nav class="footer-navigation">              
                  <p>
                    <a class="site-footer__col-one_links" href="<?= $privacy_policy_link ?>">
                      <?= $privacy_policy ?>
                    </a>
                  </p>
                  <p>
                    <a class="site-footer__col-one_links" href="<?= $regulations_link ?>">
                      <?= $regulations ?>
                    </a>
                  </p>
              </nav>
          </div>

          <div class="site-footer__col-two">
            <h6 class="site-footer--title">Kontakt</h6>
            <p>
              <a class="site-footer__link" href="mailto:<?= $mail_adress ?>">
                <?= $mail_adress ?>
              </a>
            </p>
            <p>
              <a class="site-footer__link" href="tel:<?= $phone_number ?>"><i class="fa fa-phone" aria-hidden="true"></i>
                <?= $phone_number ?>
              </a>
            </p>
          </div>
  
          <div class="site-footer__col-three">
            <h6 class="site-footer--title">Social Media</h6>
            <ul>
              <li class="social-icons"><a href="<?= $facebook_link ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
            </ul>
          </div>

        </div>
      </div>
      <div class="site-footer__under__inner container">
        <div class="site-footer__under__inner__col-one">        
            <p>© 2021 | by <a href="http://patrycjakoczwara.pl/" target="_blank">Patrycja Koczwara</a></p>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>
</body>
</html>