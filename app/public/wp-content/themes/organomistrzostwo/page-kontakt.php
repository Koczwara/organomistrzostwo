<?php
  $contact_title = get_field('contact_title');
  $contact_img = get_field('contact_img');
?>

<?php get_header(); ?>

<div class="banner">
    <div class="banner__bg-image" style="background-image: url(<?= $contact_img ?>);"></div>
    <div class="banner__content container container--narrow">
        <h1 class="banner__title">
            <?= $contact_title ?>
        </h1>
    </div>  
</div>
<div class="container contact-page">
    <div class="row">
        <div class="col">
            <h1>Kontakt</h1>
            <p class="contact-page--description">Jeżeli zainteresowała Państwa nasza oferta i realizacje, zapraszamy do skontaktowania się z Nami.</p>
            <?php echo do_shortcode('[contact-form-7 id="32" title="Contact Me"]'); ?>
        </div>
    </div>
</div>

<?php get_footer();

?>

