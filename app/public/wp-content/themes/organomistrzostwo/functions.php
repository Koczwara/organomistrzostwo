<?php

function organomistrzostwo_files() {
  wp_enqueue_style('custom-google-fonts', '//fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700&display=swap', '//fonts.googleapis.com/css2?family=Merriweather+Sans:wght@300;400;500;600;700;800&display=swap');
  wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
  
  if (strstr($_SERVER['SERVER_NAME'], 'organomistrzostwo.local')) {
    wp_enqueue_script('main-organomistrzostwo-js', 'http://localhost:3000/bundled.js', NULL, '1.0', true);
  } else {
    wp_enqueue_script('our-vendors-js', get_theme_file_uri('/bundled-assets/vendors~scripts.93f6f5a51bc67b7ecc94.js'), NULL, '1.0', true);
    wp_enqueue_script('main-organomistrzostwo-js', get_theme_file_uri('/bundled-assets/scripts.3f8ca90ad515ea7e8894.js'), NULL, '1.0', true);
    wp_enqueue_style('our-main-styles', get_theme_file_uri('/bundled-assets/styles.3f8ca90ad515ea7e8894.css'));
    wp_enqueue_style('our-vendors-styles', get_theme_file_uri('/bundled-assets/styles.3f8ca90ad515ea7e8894.css'));
  }
}

wp_localize_script('main-organomistrzostwo-js', 'organomistrzostwoData', array(
  'root_url' => get_site_url()
));

add_action('wp_enqueue_scripts', 'organomistrzostwo_files');

function organomistrzostwo_features() {
  register_nav_menu('headerMenuLocation', 'Header Menu Location');
  add_theme_support('title-tag');
}

add_action('after_setup_theme', 'organomistrzostwo_features');

add_filter('ai1wm_exclude_content_from_export', 'ignoreCertainFiles');

function ignoreCertainFiles($exclude_filters) {
  $exclude_filters[] = 'themes/organomistrzostwo/node_modules';
  return $exclude_filters;
}

function remove_cf7_assets(){
    wp_reset_query();
    if(!is_page("kontakt")){
        add_filter( 'wpcf7_load_js', '__return_false' ); 
        add_filter( 'wpcf7_load_css', '__return_false' );
    }  
}
add_action("wp", "remove_cf7_assets"); 



