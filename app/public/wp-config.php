<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'UT/u0YSLMx7tPwE555EPlxiAZf9vZkyHLjgwNkRLIFTxwt8EobcgoJMKJgz9agca5RWvqpvocgJkh/Zk/v3UmQ==');
define('SECURE_AUTH_KEY',  'EU+U8rjdFCdaNEfKUTLmJXp+VOJzJrXVXukf1w7N2weNIK8IBCBQg3JuMpRm/TaQ+feNRirHymWeKQ/UYL2eHA==');
define('LOGGED_IN_KEY',    'PqAl8zu+ItfF49vCS7xMUr1xS1TijD3qVNkLJipfCVBXB2CdLbo0hw4+pR78Q+XQqC1ZmO3cdwfS5uEgSmjR9g==');
define('NONCE_KEY',        'Zi3zcC2L4jX+6/G5CcCEor5W+UlvXXQDkV5k8EDYy6ySb25t0Nu+8n64wzUQ+vbvMRuB0A3WZ5muMr8xX2Z00g==');
define('AUTH_SALT',        'A4ohp3WMP2ib/gDT54hJbsjGaYTy9FqGyIMxNp8CK4lzBLgT98YKzveu5SlEgYoLgv5CdFNaHarAmajmbTv6UA==');
define('SECURE_AUTH_SALT', 'Up0yN4SbHYVL5tvNoCGGEhPls+2adwgZOHfxDybTJrV7ZINscgV16RLqwJXW9Ytfuhn0Yz63lpR62pI+qRcbGw==');
define('LOGGED_IN_SALT',   'E2YuQgM4HwAX4iQXfdj4+GhwC9xvzGJWdOfgoMYEGjkN9/NAtNpR66VO+ZcI1OAae6ijQeNozFmXop7vRTV+7g==');
define('NONCE_SALT',       'iCPCKbsic+9c20+ir5PonTO/gXmp5FBfMx3nvdaRF1sWEP0/B3cUY6JNmAY2pVIN7849/DaAszcCAiW9aTXcwg==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
